/**
 * Gorilla Facebook
 * 
 * @name        gorillaFB v 1.2.2
 * @date        2013-06-19
 * @developers  Tomasz Zaborski, Sebastian Bielawski
 * @company     Gorilla Media
 * @website     http://www.gorilla-media.pl
 * @homepage    https://dev.gorilla-media.pl
 */

var gorillaFB = {
    /**
     * Plugin configuration
     * 
     * @type {Object}
     */
    app_cfg: {
        debug: false,
        pattern: {
            picture: window.location.protocol + "//graph.facebook.com/[id]/picture"
        }
    },
    /**
     * Facebook configuration
     * 
     * @type {Object}
     */
    config: {
        appId: "",
        channel: "",
        status: true,
        cookie: true,
        xfbml: true,
        logging: true,
        permissions: "",
        frictionless: false
    },
    /**
     * User information
     * 
     * @type {Object}
     */
    user: {
        initAuth: {},
        details: {}
    },
    /**
     * Events list
     *
     * @type {Object}
     */
    events: {
        gorilla: [
            "event.on", "event.off", "send", "post", "oauth", "query", "photo.add", "invitation.send", "fan", "likes",
            "friends", "me", "login", "logout", "init"
        ],
        facebook: [
            "auth.login", "auth.authResponseChange", "auth.statusChange", "auth.logout", "auth.prompt",
            "xfbml.render", "edge.create", "edge.remove", "comment.create", "comment.remove", "message.send"
        ]
    },
    /**
     * Debug
     * 
     * @param {Mixed} msg
     */
    debug: function(msg) {
        if (gorillaFB.app_cfg.debug && (typeof console === "object")) {
            console.log(msg, "gorillaFB");
        }
    },
    /**
     * Check empty
     * 
     * @param {Object} obj
     * @returns {Boolean}
     */
    isEmpty: function(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    },
    /**
     * 
     * @param {Mixed} callback
     * @returns {Boolean}
     */
    isCallback: function(callback) {
        return $.isFunction(callback);
    },
    /**
     * Check permission
     * 
     * @param {String} name
     * @returns {Boolean}
     */
    hasPermission: function(name) {
        return (this.config.permissions.indexOf(name) >= 0);
    },
    /**
     * Display error
     * 
     * @param {String} message
     */
    error: function(message) {
        if (gorillaFB.app_cfg.debug) {
            alert("[ERROR] " + message);
        }
        return this;
    },
    /*
     * BASIC INIT FUNCIONALITY
     * 
     * - Facebook API AsyncInit
     * - Basic getLoginStatus response callback
     */
    init: function(config) {
        var self = this;

        // Extend configuration
        gorillaFB.config = $.extend({}, gorillaFB.config, config);

        window.fbAsyncInit = function() {
            FB.init({
                appId: self.config.appId,
                channelUrl: self.config.channel,
                status: self.config.status,
                cookie: self.config.cookie,
                xfbml: self.config.xfbml,
                logging: self.config.logging,
                frictionlessRequests: self.config.frictionless,
                oauth: self.config.oauth
            });

            /**
             * Notify auth status change
             */
            if (self.config.logging) {
                FB.Event.subscribe("authResponseChange", function(response) {
                    // Save user authorization
                    gorillaFB.user.initAuth = response;

                    // Debug response
                    gorillaFB.debug(response);

                    // Basic response handler
                    if (response.status === "connected") {
                        self.getUser();
                    } else if (response.status === 'not_authorized') {
                        gorillaFB.debug("User has not authentificated this application.");
                    } else {
                        gorillaFB.debug("User isnt logged in to Facebook.");
                    }

                    // Trigger event
                    $(window).trigger("gorillaFB.loginstatus", response);

                    gorillaFB.debug(gorillaFB);
                });
            }

            /**
             *  FB API getLoginStatus() callback
             *  http://developers.facebook.com/docs/reference/javascript/FB.getLoginStatus/
             */
            if (self.config.status) {
                FB.getLoginStatus(function(response) {
                    // Save user authorization
                    gorillaFB.user.initAuth = response;
                    // Debug purpose
                    gorillaFB.debug(response);

                    // Basic response handler
                    if (response.status === 'connected') {
                        self.getUser();
                    } else if (response.status === 'not_authorized') {
                        gorillaFB.debug("User has not authentificated this application.");
                    } else {
                        gorillaFB.debug("User isnt logged in to Facebook.");
                    }

                    // Trigger event
                    $(window).trigger("gorillaFB.loginstatus", response);

                    gorillaFB.debug(gorillaFB);
                });
            }

            // Trigger event
            $(window).trigger("gorillaFB.init");
        };
    },
    /**
     * Login to Facebook
     * 
     * @param {Function} callback
     */
    login: function(callback) {
        gorillaFB.debug("Login");
        var self = this;

        // call FB.login()
        FB.login(function(response) {
            // Debug purpose
            self.debug(response);

            // Regular FB response action
            if (response.authResponse) {
                // Save user authorization
                gorillaFB.user.initAuth = response;
                // Get user data
                self.getUser();
            } else {
                self.error("User cancelled login or did not fully authorize.");
            }

            // Run callback
            if (gorillaFB.isCallback(callback)) {
                callback(response);
            }

            // User defined callback
            $(window).trigger("gorillaFB.login", response);
            return false;
        }, {
            scope: self.config.permissions
        });

        return self;
    },
    /**
     * Logout from Facebook
     */
    logout: function(callback) {
        gorillaFB.debug("Logout");
        var self = this;

        // call FB.login()
        FB.logout(function(response) {
            // Debug purpose
            self.debug(response);

            // Run callback
            if (gorillaFB.isCallback(callback)) {
                callback(response);
            }

            // User defined callback
            $(window).trigger("gorillaFB.logout", response);
            return false;
        });

        return self;
    },
    /**
     * User details
     */
    getUser: function() {
        gorillaFB.debug("GetUser");

        var self = this;
        FB.api('/me', function(response) {
            // Save user details
            self.user.details = response;

            gorillaFB.debug(response);

            // User defined callback
            $(window).trigger("gorillaFB.me", response);
            return response;
        });

        return self;
    },
    /**
     * Get user friends
     * 
     * @param {Object} params {limit: integer, callback: function}
     */
    getFriends: function(params) {
        var self = this;

        // Default API URL
        var url = "/me/friends";

        // Check if limit parameter is present, if yes limit results
        if (params !== undefined && params.limit !== undefined) {
            // Prepare query
            url += "?limit=" + parseInt(params.limit);
        }

        // Call Facebook API
        FB.api(url, function(response) {
            // Execute user callback
            if (params !== undefined && self.isCallback(params.callback)) {
                params.callback(response);
            }

            // Trigger event
            $(window).trigger("gorillaFB.friends", response);
        });

        return self;
    },
    /**
     * 
     * 
     * @param {JSON} params
     */
    usedByFriends: function(params) {
        var self = this;

        self.query({
            query: "SELECT uid, name, is_app_user FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1=me()) AND is_app_user=1",
            callback: (params !== undefined && self.isCallback(params.callback)) ? params.callback : function() {
            }
        });

        return self;
    },
    /**
     * Get user "Like"
     * 
     * @param {Object} params
     */
    getLikes: function(params) {
        var self = this;

        // Check if application has required permission to execute getLikes()
        if (self.hasPermission("user_likes")) {
            // Default API URL
            var url = "/me/likes";

            // Check if limit parameter is present, if yes limit results
            if (params !== undefined && params.limit !== undefined) {
                url = url + "?limit=" + parseInt(params.limit);
            }

            // Call Facebook API
            FB.api(url, function(response) {
                // Check if user defined callback
                if (params !== undefined && self.isCallback(params.callback)) {
                    // Execute user callback
                    params.callback(response);
                }

                // Trigger event
                $(window).trigger("gorillaFB.likes", response);
            });
        } else {
            // Display error
            self.error("gorillaFB.getLikes : 'user_likes' permission required to get user likes!");
        }

        return self;
    },
    /**
     * User is fan
     * 
     * @param {Object} params {id : integer, assignTo: jQuery, callback: function}
     */
    isFan: function(params) {
        var self = this;

        // Check if application has required permission to execute 
        if (self.hasPermission("user_likes")) {
            var url = "/me/likes";

            // Check if page ID provided
            if (params !== undefined && params.id !== undefined) {
                url += "/" + params.id;
            }

            //
            var _isFan = false;
            FB.api(url, function(response) {
                // Regular action
                if (response.data) {
                    if (!self.isEmpty(response.data)) {
                        _isFan = true;
                    }
                } else {
                    self.error("Unable to check if user liked page.");
                }

                if (params !== undefined) {
                    // Check for assignTo parameter
                    if (params.assignTo !== undefined) {
                        // Execute user callback
                        window[params.assignTo] = _isFan;
                    }

                    // Check if user defined callback
                    if (self.isCallback(params.callback)) {
                        // Execute user callback
                        params.callback(response);
                    }

                    // Trigger event
                    $(window).trigger("gorillaFB.fan", response);
                }
            });
        } else {
            self.error("You don't have 'user_likes' permission.");
        }

        return self;
    },
    /**
     * Facebook events
     */
    Event: {
        /**
         * Bind event
         *
         * @param {String} name
         * @param {Object} callback
         */
        on: function(name, callback) {
            gorillaFB.debug("Event.on");

            // Check if event exists
            if ($.inArray(name, gorillaFB.events.facebook) === false) {
                this.error("Event by name '" + name + "' not found.");
                return this;
            }

            var self = this;

            FB.Event.subscribe(name, function(response) {
                // Run callback
                if (self.isCallback(callback)) {
                    callback(response);
                }

                // Fire event
                $(window).trigger("gorillaFB.eventOn", {
                    name: name,
                    response: response
                });
            });

            return self;
        },
        /**
         * Unbind event
         *
         * @param {String} name
         * @param {Object} callback
         */
        off: function(name, callback) {
            gorillaFB.debug("Event.off");

            // Check if event exists
            if ($.inArray(name, gorillaFB.events.facebook) === false) {
                this.error("Event by name '" + name + "' not found.");
                return this;
            }

            var self = this;

            FB.Event.unsubscribe(name, function(response) {
                // Run callback
                if (self.isCallback(callback)) {
                    callback(response);
                }

                // Fire event
                $(window).trigger("gorillaFB.eventOff", {
                    name: name,
                    response: response
                });
            });

            return self;
        }
    },
    /**
     * Invitations
     */
    Invitation: {
        /**
         * Send invitation to friends
         * 
         * @param {Object} params
         */
        send: function(params) {
            gorillaFB.debug("Invitation.send");
            var self = this;

            if (params === undefined) {
                self.error("gorillaFB.Invitation.send : Params not defined");
                return self;
            }

            FB.ui({
                method: "apprequests",
                message: params.message
            }, function(response) {
                // Check if user defined callback
                if (self.isCallback(params.callback)) {
                    // Execute user callback
                    params.callback(response);
                }

                // Trigger event
                $(window).trigger("gorillaFB.invitationSend", response);
            });
        }
    },
    /**
     * Photo subclass
     */
    Photo: {
        /**
         * Add photo
         * 
         * @param {JSON} params
         */
        add: function(params) {
            gorillaFB.debug("Photo.add");
            var self = this;

            if (params === undefined) {
                self.error("gorillaFB.Photo.add : Params not defined");
                return this;
            }

            // Check if album ID provided?
            var url = "";
            if (params.albumID !== undefined) {
                url = "/" + params.albumID;
            }

            url += "/photos";

            // Prepare Facebook POST request
            FB.api(url, 'post', {
                message: params.description, // Image description
                url: params.url          // Image URL
            }, function(response) {
                // Check for error
                if (!response || response.error) {
                    self.error('Error occured. Photo was not uploaded.');
                }

                // Callback?
                if (self.isCallback(params.callback)) {
                    // Execute user callback
                    params.callback(response);
                }

                // Trigger event
                $(window).trigger("gorillaFB.photoAdd", response);
            });

            return self;
        },
        /*
         *  FB API
         *  
         *  Returns url to photo of: user, album, page, app
         *  @data.type      string  | square, small, normal, large
         *  @data.width     int     | 0-200
         *  @data.height    int     | 0-200 
         */
        get: function(data) {
            gorillaFB.debug("Photo.get");
            var self = this;

            // Make default photo ID
            if (data.id === undefined) {
                data.id = self.user.details.id;
            }

            var url = self.app_cfg.pattern.picture.replace('[id]', data.id);

            if (data.type === undefined) {
                // Provided dimensions
                url += "?width=" + parseInt(data.width) + "&height=" + parseInt(data.height);
            } else {
                switch (data.type) {
                    default:
                        url += "?type=square";
                        break;
                    case "small":
                        url += "?type=small";
                        break;
                    case "normal":
                        url += "?type=normal";
                        break;
                    case "large":
                        url += "?type=large";
                        break;
                }
            }

            return url;
        }
    },
    /*
     * FB API, FQL functionalities
     * 
     * @param {Object} params {query: "", callback: function}
     */
    query: function(params) {
        gorillaFB.debug("Query");
        var self = this;

        if (params === undefined) {
            self.error("gorillaFB.query : Params not defined");
            return self;
        }

        FB.api("fql",
                {
                    q: params.query
                },
        function(response) {
            // Execute user callback
            if (self.isCallback(params.callback)) {
                // Execute callback
                params.callback(response);
            }

            // Trigger event
            $(window).trigger("gorillaFB.query", response);
        });

        return self;
    },
    /**
     * Application authorize
     */
    oauth: function() {
        gorillaFB.debug("OAuth");
        var self = this;

        var options = {
            method: "oauth",
            response_type: "token"
        };

        FB.ui(options, function(response) {
            self.getUser();

            // Trigger event
            $(window).trigger("gorillaFB.oauth", response);
        });

        return self;
    },
    /**
     * User Wall/Timeline/Facebook functionalities
     * 
     * @param {Object} params
     */
    post: function(params) {
        gorillaFB.debug("Post");
        var self = this;
        var defaults = {
            method: "feed",
            name: "name",
            link: "picture",
            caption: "caption",
            description: "description"
        };

        var options = $.extend({}, defaults, params);

        FB.ui(options, function(response) {
            // Execute user callback
            if (params !== undefined && self.isCallback(params.callback)) {
                params.callback(response);
            }

            // Trigger event
            $(window).trigger("gorillaFB.post", response);
        });

        return self;
    },
    /**
     * Sending message
     * 
     * @param {Object} params {name: "", description: "", link: "", to: "", picture: ""}
     */
    send: function(params) {
        gorillaFB.debug("Send");
        var self = this;

        var options = {
            method: "send",
            to: "",
            name: "",
            description: "",
            picture: "",
            link: ""
        };

        options = $.extend({}, options, params);

        FB.ui(options, function(response) {
            // Execute user callback
            if (self.isCallback(params.callback)) {
                params.callback(response);
            }

            // Trigger event
            $(window).trigger("gorillaFB.send", response);
        });

        return self;
    },
    /**
     * Bind event
     * 
     * @param {String} eventName
     * @param {Function} callback
     */
    on: function(eventName, callback) {
        gorillaFB.debug("On");

        // Check if event exists
        if ($.inArray(eventName, gorillaFB.events.gorilla) === false) {
            this.error("Event by name '" + eventName + "' not found.");
            return this;
        }

        // Run callback
        if (this.isCallback(callback) === false) {
            this.error("gorillaFB.on : Callback needed");
        }

        // Fire event
        $(window).on("gorillaFB." + eventName, callback);
        return this;
    },
    /**
     * Unbind event
     * 
     * @param {String} eventName
     */
    off: function(eventName) {
        gorillaFB.debug("Off");

        // Check if event exists
        if ($.inArray(eventName, gorillaFB.events.gorilla) === false) {
            this.error("Event by name '" + eventName + "' not found.");
            return this;
        }

        // Fire event
        $(window).off("gorillaFB." + eventName);
        return this;
    }
};

